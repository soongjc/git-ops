### 1. Create a Secret

You can create a secret by creating a YAML file, for example `keycloak-db-secret.yaml`, and then applying it with `kubectl apply`. Ensure to base64 encode your actual username and password before creating the secret.

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: keycloak-secret
type: Opaque
data:
  # Replace the base64 encoded values accordingly
  db-user: [your-base64-encoded-username]
  db-password: [your-base64-encoded-password]
```
Encode your actual username and password with base64 and replace `[your-base64-encoded-username]` and `[your-base64-encoded-password]` accordingly. Here's how you can base64 encode your values in a shell:

```shell
echo -n 'your-actual-username' | base64
echo -n 'your-actual-password' | base64
```

After creating `keycloak-secret.yaml`, apply it using `kubectl`:

```shell
kubectl apply -f keycloak-secret.yaml
```

### 2. Reference the Secret in Your Deployment

Modify your deployment to use the secrets stored in `keycloak-db-secret`:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: keycloak
  labels:
    app: keycloak
spec:
  replicas: 1
  selector:
    matchLabels:
      app: keycloak
  template:
    metadata:
      labels:
        app: keycloak
    spec:
      containers:
        - name: keycloak
          image: quay.io/keycloak/keycloak:$$VERSION$$
          args: ["start-dev"]
          env:
            - name: KEYCLOAK_ADMIN
              value: "admin"
            - name: KEYCLOAK_ADMIN_PASSWORD
              value: "admin"
            - name: KC_PROXY
              value: "edge"
            # Database configuration
            - name: DB_VENDOR
              value: "postgres"
            - name: DB_ADDR
              value: "your-postgres-ip-or-hostname"
            - name: DB_PORT
              value: "5432"
            - name: DB_DATABASE
              value: "keycloak"
            - name: DB_USER
              valueFrom:
                secretKeyRef:
                  name: keycloak-db-secret
                  key: db-user
            - name: DB_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: keycloak-db-secret
                  key: db-password
          ports:
            - name: http
              containerPort: 8080
          readinessProbe:
            httpGet:
              path: /realms/master
              port: 8080
```