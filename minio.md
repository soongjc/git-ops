helm install \
--namespace minio-operator \
--create-namespace \
minio-operator operator-5.0.10.tgz

kubectl get service console -n minio-operator -o yaml > service.yaml
yq e -i '.spec.type="NodePort"' service.yaml
yq e -i '.spec.ports[0].nodePort = 32323' service.yaml

kubectl get deployment minio-operator -n minio-operator -o yaml > operator.yaml
yq -i -e '.spec.replicas |= 1' operator.yaml

kubectl get all --namespace minio-operator

SA_TOKEN=$(kubectl -n minio-operator  get secret console-sa-secret -o jsonpath="{.data.token}" | base64 --decode)
echo $SA_TOKEN

kubectl --namespace minio-operator port-forward svc/console 9090:9090

helm install \
--namespace tenant-ns \
--create-namespace \
tenant-ns tenant-5.0.10.tgz

kubectl --namespace tenant-ns port-forward svc/myminio-console 9443:9443