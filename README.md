# README: Setting Up HTTPS for a Service in GKE (Google Kubernetes Engine)

## Overview

This comprehensive guide will walk you through the steps to set up HTTPS for a service running in Google Kubernetes Engine (GKE) using NGINX Ingress Controller and Let's Encrypt for SSL certificate management. Please follow the steps accurately to replicate this setup in your own environment.

### Prerequisites

- A running GKE cluster
- `kubectl` installed and configured to interact with your GKE cluster
- `helm` v3.x installed 

## Step by Step Guide

### Step 1: Add NGINX Ingress Helm Repository

The first step involves adding the NGINX Ingress Helm repository to your Helm CLI. This will allow you to easily install the NGINX Ingress Controller on your GKE cluster.

```shell
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
```

### Step 2: Install NGINX Ingress Controller

Install the NGINX Ingress Controller using Helm. This controller will manage the ingress objects in your Kubernetes cluster and facilitate the routing of traffic to your services.

```shell
helm install appbackend-ingress ingress-nginx/ingress-nginx
```

### Step 3: Verify NGINX Ingress Controller Installation

Ensure that the ingress controller is running and note the external IP provided by the service. This IP will be used to point your domain DNS to the ingress controller.

```shell
kubectl --namespace default get services -o wide -w appbackend-ingress-ingress-nginx-controller
```

### Step 4: Set Up Let's Encrypt Issuer

Apply the `letsencrypt-issuer.yaml` manifest to set up the Let's Encrypt issuer. This issuer will facilitate the automatic provisioning and renewal of certificates issued by Let's Encrypt.

```shell
kubectl apply -f manifests/letsencrypt-issuer.yaml
```

*Note*: Ensure your `letsencrypt-issuer.yaml` manifest specifies whether you want to use Let's Encrypt's staging or production environment.

**Example: letsencrypt-issuer.yaml**

```yaml
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt
spec:
  acme:
    # Server URL for Let's Encrypt's staging environment
    # For production, use: https://acme-v02.api.letsencrypt.org/directory
    server: https://acme-staging-v02.api.letsencrypt.org/directory
    email: [your-email]
    privateKeySecretRef:
      name: letsencrypt
    solvers:
    - http01:
        ingress:
          class: nginx
```

### Step 5: Define Ingress Resource

Apply the `appbackend-ingress.yaml` manifest to create the ingress resource. Ensure to specify the backend service, paths, and host (your domain) correctly.

```shell
kubectl apply -f manifests/appbackend-ingress.yaml
```

**Example: appbackend-ingress.yaml**

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: appbackend-ingress
spec:
  rules:
  - host: example.com
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: appbackend-service
            port:
              number: 80
  tls:
  - hosts:
    - example.com
    secretName: appbackend-tls
```

### Step 6: Create Certificate

Apply the `certificate.yaml` manifest to create a certificate resource. Cert-manager will use the previously created issuer to request a certificate from Let's Encrypt.

```shell
kubectl apply -f manifests/certificate.yaml
```

**Example: certificate.yaml**

```yaml
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: appbackend-tls
spec:
  secretName: appbackend-tls
  duration: 2160h # 90d
  renewBefore: 360h # 15d
  issuerRef:
    name: letsencrypt
    kind: ClusterIssuer
  commonName: example.com
  dnsNames:
  - example.com
```

### Step 7: Validate Setup

Verify that your setup works by navigating to `https://example.com` (replace `example.com` with your actual domain). Ensure that traffic is routed correctly and SSL/TLS is working as expected.

## Conclusion

Congratulations! You've successfully configured HTTPS for your service on GKE. The setup consists of an NGINX Ingress controller managing traffic routing and Cert-manager handling SSL/TLS certificates via Let's Encrypt. Ensure to validate the setup and monitor the certificate renewal processes to maintain a secure environment.

If you encounter any issues, review the logs for the ingress-nginx and cert-manager pods:

```shell
kubectl logs -l app=ingress-nginx -n [namespace]
kubectl logs -l app=cert-manager -n [namespace]
```

Should you require additional configurations like redirecting HTTP to HTTPS or setting up additional routes, adapt the ingress resource definitions as per your use case.

Ensure to store these manifests in a version-controlled repository for future reference and further iterations.